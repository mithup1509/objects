function keys(obj){
    if(typeof obj !=='object'){
        return [];
    }

    let arr=[];
    for(let value in obj){
       arr.push(value);
    }
    return arr;
}
module.exports=keys;