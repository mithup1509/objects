function invert(obj) {
  if (typeof obj !== 'object') {
    return {};
  }

  let newObj = {};
  for (let value in obj) {
    // console.Klog(value);
    newObj[obj[value]] = value;
  }
  return newObj;
}

module.exports = invert;