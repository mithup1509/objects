function pairs(obj){
    if(typeof obj !== 'object'){
        return [];
    }
    let arr=[];
    for(let value in obj){
      arr.push([value,obj[value]]);
    }
    return arr;
}

module.exports=pairs;