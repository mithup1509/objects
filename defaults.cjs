function defaults(obj,defaultProps){
    if(typeof obj !== 'object' || typeof defaultProps !== 'object' ){
        return {};
    }

    for(let value in defaultProps){
        if(obj[value] ===undefined){
            obj[value]=defaultProps[value];
        }
    }

 

    return obj;
}



module.exports=defaults;