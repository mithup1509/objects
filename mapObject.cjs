function mapObject(obj,cb){
    if(typeof obj !== 'object'){
        return {};
    }
    let newObj={...obj};
    for(let value in obj){
        newObj[value]=cb(value,obj[value]);
    }
    return newObj;
}
module.exports=mapObject;